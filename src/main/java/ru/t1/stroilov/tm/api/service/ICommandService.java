package ru.t1.stroilov.tm.api.service;


import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.model.Command;

public interface ICommandService extends ICommandRepository {

    Command[] getCommandsArray();

}