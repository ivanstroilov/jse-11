package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public ProjectRepository() {
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> result = new ArrayList<>(projects);
        result.sort(comparator);
        return result;
    }

    @Override
    public void deleteAll() {
        projects.clear();
    }

    @Override
    public boolean existsById(final String id) {
        return findByID(id) != null;
    }

    @Override
    public Project findByID(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project delete(final Project project) {
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project deleteByID(final String id) {
        final Project project = findByID(id);
        if (project == null) return null;
        return delete(project);
    }

    @Override
    public Project deleteByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        return delete(project);
    }
}
